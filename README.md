# RR-0 µController Code

This REPO is for microcontroller code for RR-0.

## Overview

The microcontroller reads low-level sensor data from IMUs on the robot and transmits them to the computer. The microcontroller also receives joint trajectories from the computer and sends them via PWM to our low-cost servo motors.

## Data Formats

Data will be transmitted using the [WXF binary data exchange format](https://reference.wolfram.com/language/tutorial/WXFFormatDescription.html). This format is a self-describing binary format designed for efficiently transmitting many different kinds of scientific data.

### Joint data

Joint data will be encoded using a WXF 8-bit integer array.

WXF Array data looks like this:

```
Â^<numericarray-type>^<rank>^<array-dims>^<numericarray-data>
```

The entire joint data message (including a header `8:`) will look like this
```
8:Â<0><1><12><j1><j2><...><j12>
```

Where for each of the `<12>` motors, each joint target angle `<jX>` will be represented by a signed integer on $[-128, 127]$.
