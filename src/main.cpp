#include <Arduino.h>


void ISR_receive_servo_goal(void) {
  if (Serial.available() > 0) {
    // Read the incoming byte
    int incomingByte = Serial.read();
    // Write to servo here
    // Example: setServoPosition(incomingByte);
  }
}

void check_for_safety_signal(void);
void listen_for_serial_msg(void)
{
  if (Serial.available() > 0)
  {
    ISR_receive_servo_goal();
  }
}


int main(void)
{

  Serial.begin(9600);


  for(;;)
  {
    check_for_safety_signal();
    listen_for_serial_msg();
  }

  return 1;
}

